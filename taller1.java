import java.util.Arrays;
import java.util.Scanner;

public class taller1{
    //option class 
    public static class options{
        //scanner object
        Scanner sc = new Scanner(System.in);
        //menu function, recive a array of numbers int[]a, the num of array h and the array d
        public int[] menu(int[] a,int h,luisarray d){
            boolean keep = true;
            while(keep){
                //shoe the options
                System.out.println("");
                System.out.println("1. Define size of Array " + h + "\n" + "2. add a data Array " + h + "\n" + "3. delete a field en el Array " + h + "\n" + "4. add a field en el Array " + h + "\n" + "5. Back");
                //get the option
                int o2 =sc.nextInt();
                //select the correct option
                switch(o2){
                    case 1:
                        //create a array of the desired size
                        System.out.println("Put the size of Array " + h +":");
                        int tam = sc.nextInt();
                        d = new luisarray(tam);
                    break;
                    case 2:
                        // full the array with data
                        if(!(d.getsize() >0)){
                            System.out.println("you did not defined the array size, use the option 1");
                        }else{
                            System.out.println("Put the elements on acended order");
                            for(int i=0;i<d.getsize();i++){
                                System.out.println("put the value #" + (i+1));
                                int num = sc.nextInt();
                                d.add(num,i);
                            }
                        }
                    break;
                    case 3:
                        //delete a item of the array if the position exist
                        System.out.println("put the index of the item to delete:");
                        int p = sc.nextInt();
                        p--;
                        if(p>=0 && p<d.size){
                            d.delete(p);
                        }else{
                            System.out.println("This index dont exist");
                        }
                    break;
                    case 4:
                        //add a new data on the desired position and move the elements to put the data
                        System.out.println("Put the index to add the data");
                        int index = sc.nextInt();
                        index--;
                        if(index <= d.getsize()){
                            //select the desired array
                            System.out.println("Do you want 1. put a new data or 2. emply");
                            int o3 = sc.nextInt();
                            switch(o3){
                                case 1:
                                    System.out.println("Put the new data:");
                                    int da = sc.nextInt();
                                    d.addfield(index, da);
                                break;
                                case 2:
                                    d.addfield(index, 0);
                                break;
                            }
                        }else{
                            System.out.println("the index must be between 1 and " + (d.getsize()+1));
                        }
                    break;
                    case 5:
                        // exit
                        keep=false;
                    break;
                    default:
                        System.out.println("this option dont exist put a correct one");
                    break;
                }
                //print the array after every operation if it exist
                if(d.getsize() !=-1){
                    d.print();
                    System.out.print(" ");
                    d.printsize();
                }
            }
            //return the final array
            return d.arr;
        }
    }
    //the personalised array class, it extend the class options with the method menu
    public static class luisarray extends options{
            // inicialize the num array
            int[] arr;
            //put the size on -1 until the array start
            int size =-1;
            //constructor with the size of array
            public luisarray(int s){
                arr = new int[s];
                this.size = s;
            }
            //constructor emply
            public luisarray(){
            }
            // override of the menu method of options and recive the array number
            public void menu(int j) {
                //use the menu method
                this.arr = super.menu(this.arr,j,this);
                if(arr.length>0){
                    this.size =arr.length;
                }
            }
            //method to add data on a determinated pos
            public void add(int data, int pos){
                arr[pos] = data;
            }
            //return the size of the array
            public int getsize(){
                return this.size;
            }
            //print the size of array
            public void printsize(){
                System.out.println("Size: " + this.size);
            }
            //return the element on a terminated pos j
            public int getpos(int j){
                return arr[j];
            }
            //return the array of numebers
            public int[] getarray(){
                return arr;
            }
            //delete a element on a determinated position
            public void delete(int pos){
                int[] n = new int[arr.length-1];
                for(int i=0;i<pos;i++){
                   n[i] = arr[i]; 
                }
                for(int j=pos;j<n.length;j++){
                    n[j]=arr[j+1];
                }
                arr = n;
                size--;
            }
            //add a field and put on there a new data
            public void addfield(int pos,int da){
                boolean h =false;
                int[] n = new int[arr.length+1];
                for(int i=0;i<n.length;i++){
                    if(i == pos){
                        n[i] = da;
                        h = true;
                    }else{
                        if(h){
                            n[i] = arr[i-1];
                        }else{
                            n[i] = arr[i];
                        }
                        
                    }
                }
                arr = n;
                size++;
            }
            //print the array
            public void print(){
                System.out.print("[");
                for(int i=0;i<arr.length;i++){
                    if(i != arr.length-1){
                        System.out.print(arr[i] + ", ");
                    }else{
                        System.out.print(arr[i] + " ]");
                    }
                    
                }
                System.out.println("");
            }
    }
    public static class operations{
        //inicializate scanner 
        Scanner sc = new Scanner(System.in);
        //emply contructor 
        public operations(){

        }
        //method to select the action, recbe the arrays to work lu1,lu2
        public void action(luisarray lu1,luisarray lu2){
            //keep goinig 
            boolean kg = true;
            //cicle
            while(kg){
                //present options
                System.out.print("\n" +"1. Sum 2 arrays" + "\n" + "2. Substract 2 arrays" + "\n" + "3. Multiply 2 arrays" + "\n" +"4. Factorial of a array" + "\n" + "5. Fibonnaci of a array" + "\n" + "6. Back" + "\n");
                //recive the option
                int o3 =sc.nextInt();
                //select the correct option
                switch(o3){
                    case 1:
                    if(lu1.getsize() >0 && lu2.getsize() >0)
                        sum(lu1,lu2);
                    else
                        System.out.println("a Array is not inicializated");
                    break;
                    case 2:
                    if(lu1.getsize() >0 && lu2.getsize() >0)
                        subs(lu1, lu2);
                    else
                        System.out.println("a Array is not inicializated");
                    break;
                    case 3:
                    if(lu1.getsize() >0 && lu2.getsize() >0)
                        mul(lu1,lu2);
                    else
                        System.out.println("a Array is not inicializated");
                    break;
                    case 4:
                        //select the array to aply the operation
                        System.out.println("Witch array?");
                        int o4 =sc.nextInt();
                        switch(o4){
                            case 1:
                                Factorial(lu1);
                            break;
                            case 2:
                                Factorial(lu2);
                            break;
                            default:
                                System.out.println("this option dont exist");
                            break;
                        }
                        
                    break;
                    case 5:
                        //select the number to apply the operation
                        System.out.print("Select the number of array:");
                        int o5 =sc.nextInt();
                        switch(o5){
                            case 1:
                                fib(lu1);
                            break;
                            case 2:
                                fib(lu2);
                            break;
                            default:
                                System.out.println("this option dont exist");
                            break;
                        }
                    break;
                    case 6:
                    kg =false;
                    break;
                    default:
                        //in case of the user use a no availble option
                        System.out.println("this option dont exist");
                    break;
                }
            }
        }
        //method to sum the arrays elements, recive the arrays to aply the operation (l1,l2) and print the result
        public void sum(luisarray l1, luisarray l2){
            String[] arrfinal;
            boolean ste = true;
            //System.out.print("l1: "+l1.getsize() + " l2: " + l2.getsize());
            //select the size of aaray to print
            if(l1.getsize() > l2.getsize()){
                arrfinal = new String[l1.getsize()];
                ste = true;
            }else{
                arrfinal = new String[l2.getsize()];
                ste = false;
            }
            //put the element on a correct way
            for(int i = 0;i<arrfinal.length;i++){
                if(ste){
                    if(i<l2.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)+l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }else{
                    if(i<l1.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)+l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }
            }
            System.out.println("Array 1:");
            l1.print();
            System.out.println("");
            System.out.println("Array 2:");
            l2.print();
            System.out.println("");
            System.out.print("Resultado: "+ "\n" + Arrays.toString(arrfinal));
        }
        //method to substract the arrays elements, recive the arrays to aply the operation (l1,l2) and print the result
        public void subs(luisarray l1, luisarray l2){
            String[] arrfinal;
            boolean ste = true;
            //System.out.print("l1: "+l1.getsize() + " l2: " + l2.getsize());
            //select the size of aaray to print
            if(l1.getsize() > l2.getsize()){
                arrfinal = new String[l1.getsize()];
                ste = true;
            }else{
                arrfinal = new String[l2.getsize()];
                ste = false;
            }
            //put the element on a correct way
            for(int i = 0;i<arrfinal.length;i++){
                if(ste){
                    if(i<l2.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)-l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }else{
                    if(i<l1.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)-l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }
            }
            System.out.println("Array 1:");
            l1.print();
            System.out.println("");
            System.out.println("Array 2:");
            l2.print();
            System.out.println("");
            System.out.print("Resultado: "+ "\n" + Arrays.toString(arrfinal));
        }
        //method to multiply the arrays elements, recive the arrays to aply the operation (l1,l2) and print the result
        public void mul(luisarray l1, luisarray l2){
            String[] arrfinal;
            boolean ste = true;
            //select the size of aaray to print
            if(l1.getsize() > l2.getsize()){
                arrfinal = new String[l1.getsize()];
                ste = true;
            }else{
                arrfinal = new String[l2.getsize()];
                ste = false;
            }
            //put the element on a correct way
            for(int i = 0;i<arrfinal.length;i++){
                if(ste){
                    if(i<l2.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)*l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }else{
                    if(i<l1.getsize()){
                        arrfinal[i] = Integer.toString(l1.getpos(i)*l2.getpos(i));
                    }else{
                        arrfinal[i] = "E";
                    }
                }
            }
            System.out.println("Array 1:");
            l1.print();
            System.out.println("");
            System.out.println("Array 2:");
            l2.print();
            System.out.println("");
            System.out.print("Resultado: "+ "\n" + Arrays.toString(arrfinal));
        }
        //method to realize the factorial, recive the array l1
        public void Factorial(luisarray l1){
            String[] finalprint = new String[l1.getsize()];
            for(int i = 0;i<l1.getsize();i++){
                //call the recursive option
                finalprint[i] = Integer.toString(factorialnumber(l1.getpos(i)));
            }
            System.out.print("Result:");
            System.out.print(Arrays.toString(finalprint));
        }
        //recursive function of the factorial
        public int factorialnumber(int a){
            if(a<0){
                System.out.print("the element" + a + "is a negative so his factorial dont exist, so we put a 0");
                return 0;
            }
            if(a==1 || a==0){
                return 1;
            }
            return a*factorialnumber(a-1);
        }
        // method to realize the fibonacci of a element of the array 
        public void fib(luisarray e){
            e.print();
            //select the elemtent
            System.out.println("Select the index of the item of the fibonacci's position:");
            int a = sc.nextInt();
            a = e.getpos(a-1);
            if(a>0){
                System.out.println("The fibonacci's position " + a + " is: ");
                //call the fibonacci recursive function
                int s = fibbonaci(a);
                System.out.println(s);
                if(s ==0){
                    System.out.println("the fibonacci of a negative position dont exist");
                }
            }else{
                System.out.println("the number must be a positive number");
            }
        }
        //fibonacci recurcive option, return the fibonacci on the position n
        public int fibbonaci(int w){
            if(w<0){
                return 0;
            }
            if(w == 1 || w == 2){
                return 1;
            }
            return fibbonaci(w-1) + fibbonaci(w-2);
        }
    }
    //main program
    public static void main(String[] args) {
        //open scanner
        Scanner sc = new Scanner(System.in);
        //inicializate arrays
        luisarray a1 = new luisarray();
        luisarray a2 = new luisarray();
        //inicialuzate operations
        operations ope = new operations();
        //cicle
        boolean keepgoing = true;
        while(keepgoing){
            //clean the screen
            clearScreen();
            //main menu
            System.out.println("\n" +"1. Array 1" + "\n" + "2. Array 2" + "\n" + "3. Operations" + "\n" + "4. Get out");
            //option selected
            int o = sc.nextInt();
            //options selector
            switch(o){
                case 1:
                    a1.menu(1);
                break;
                case 2:
                    a2.menu(2);
                break;
                case 3:
                    // veryfy if the arrays is iniziliced
                    if(a1.getsize()>-1){
                        System.out.println("Array 1:");
                        a1.print();
                        System.out.println("");
                    }else{
                        System.out.println("the Array 1 dont exist yet");
                    }
                    if(a2.getsize()>-1){
                        System.out.println("Array 2:");
                        a2.print();
                    }else{
                        System.out.println("the Array 2 dont exist yet");
                    }
                    //go to the operation menu
                    ope.action(a1, a2);
                    
                break;
                case 4:
                    //finish progrma
                    System.out.println("Good bye");
                    sc.close();
                    keepgoing = false;
                break;
                default:
                    //error if the user puts a no availble option
                    System.out.println("this option dont exist");
                break;
            }
        }
    }
    //clear screen functin
    public static void clearScreen() {  
        System.out.print("\033[H\033[2J");  
        System.out.flush();  
    }  
}